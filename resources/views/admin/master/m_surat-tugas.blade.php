@extends('layouts.admin._master-admin')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Master /</span> Surat Tugas </h4>

        <!-- DataTable with Buttons -->
        <div class="card">
            <div class="card-datatable table-responsive pt-0">
                <table class="datatables-surat-tugas table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Perusahaan</th>
                            <th>No. SPBU</th>
                            <th>Pengelola</th>
                            <th>No. HP</th>
                            <th>NIB</th>
                            <th>UTTP</th>
                            <th>Pertamina</th>
                            <th>Sisa Hari</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--/ DataTable with Buttons -->

        <!-- Edit User Modal -->
        {{-- <div class="modal fade" id="addSuratModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-simple modal-edit-user">
                <div class="modal-content p-3 p-md-5">
                    <div class="modal-body py-3 py-md-0">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        <div class="text-center mb-4">
                            <h3 class="mb-2">Form Tambah Data Surat Tugas SPBU</h3>
                            <p class="pt-1">Silahkan lengkapi kebutuhan data dibawah ini.</p>
                        </div>
                        <form id="editSpbuForm" class="row g-4" onsubmit="return false">
                            <div class="col-12 col-md-6">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="nomor_surat" name="nomor_surat" class="form-control"
                                        placeholder="01992023" />
                                    <label for="nomor_surat">Nomor Surat</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="objek_pengawasan" name="objek_pengawasan" class="form-control"
                                        placeholder="SPBU" />
                                    <label for="objek_pengawasan">Objek Pengawasan</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="tentang" name="tentang" class="form-control"
                                        placeholder="Masukkan rincian mengenai surat tugas yang akan dibuat." />
                                    <label for="tentang">Tentang</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="input-group input-group-merge">
                                    <div class="form-floating form-floating-outline">
                                        <input type="text" id="tanggal_surat" name="tanggal_surat"
                                            class="form-control phone-number-mask" placeholder="20-08-2023" />
                                        <label for="tanggal_surat">Tanggal Surat</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="tanggal_pengawasan" name="tanggal_pengawasan"
                                        class="form-control" placeholder="28-08-2023" />
                                    <label for="tanggal_pengawasan">Tanggal Pengawasan</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="sarana" name="sarana" class="form-control"
                                        placeholder="Mobil Innova 1 Unit" />
                                    <label for="sarana">Sarana</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="berkas_surat_tugas" name="berkas_surat_tugas"
                                        class="form-control" placeholder="Upload Berkas Surat Tugas" />
                                    <label for="berkas_surat_tugas">Berkas Surat Tugas</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="nama_perusahaan" name="nama_perusahaan" class="form-control"
                                        placeholder="PT. AJI KUSUMA SPBU" />
                                    <label for="nama_perusahaan">Nama Perusahaan</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="alamat" name="alamat" class="form-control"
                                        placeholder="Jalan Ir. Juanda No 12, Jakarta" />
                                    <label for="alamat">Alamat</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="nama_pengelola" name="nama_pengelola" class="form-control"
                                        placeholder="AJI KUSUMA" />
                                    <label for="nama_pengelola">Nama Pengelola</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="input-group input-group-merge">
                                    <span class="input-group-text">ID (+62)</span>
                                    <div class="form-floating form-floating-outline">
                                        <input type="text" id="nomor_handphone" name="nomor_handphone"
                                            class="form-control phone-number-mask" placeholder="0811 2233 3445" />
                                        <label for="nomor_handphone">Nomor Handphone</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                    aria-label="Close">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> --}}
        <!--/ Edit User Modal -->

        <!-- Create App Modal -->
        <div class="modal fade" id="createApp" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-simple modal-upgrade-plan">
                <div class="modal-content p-3 p-md-5">
                    <div class="modal-body p-1">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        <div class="text-center">
                            <h3 class="mb-2 pb-1">Form Tambah Data Surat Tugas SPBU</h3>
                            <p class="mb-4">Silahkan lengkapi kebutuhan data dibawah ini.</p>
                        </div>
                        <!-- Property Listing Wizard -->
                        <div id="wizard-create-app" class="bs-stepper vertical wizard-vertical-icons mt-2 shadow-none">
                            <div class="bs-stepper-header border-0 p-1">
                                <div class="step" data-target="#details">
                                    <button type="button" class="step-trigger">
                                        <span class="avatar">
                                            <span class="avatar-initial rounded-2">
                                                <i class="mdi mdi-content-paste mdi-24px"></i>
                                            </span>
                                        </span>
                                        <span class="bs-stepper-label flex-column align-items-start gap-1 ps-1 ms-2">
                                            <span class="bs-stepper-title text-uppercase fw-normal">Informasi Surat</span>
                                            <small class="bs-stepper-subtitle text-muted">Lengkapi Informasi</small>
                                        </span>
                                    </button>
                                </div>
                                <div class="step" data-target="#frameworks">
                                    <button type="button" class="step-trigger">
                                        <span class="avatar">
                                            <span class="avatar-initial rounded-2">
                                                <i class="mdi mdi-star-outline mdi-24px"></i>
                                            </span>
                                        </span>
                                        <span class="bs-stepper-label flex-column align-items-start gap-1 ps-1 ms-2">
                                            <span class="bs-stepper-title text-uppercase fw-normal">Petugas</span>
                                            <small class="bs-stepper-subtitle text-muted">Lengkapi Detail Petugas</small>
                                        </span>
                                    </button>
                                </div>
                                <div class="step" data-target="#submit">
                                    <button type="button" class="step-trigger">
                                        <span class="avatar">
                                            <span class="avatar-initial rounded-2">
                                                <i class="mdi mdi-check mdi-24px"></i>
                                            </span>
                                        </span>
                                        <span class="bs-stepper-label flex-column align-items-start gap-1 ps-1 ms-2">
                                            <span class="bs-stepper-title text-uppercase fw-normal">Submit</span>
                                            <small class="bs-stepper-subtitle text-muted">Selesai</small>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <div class="bs-stepper-content p-1">
                                <form onSubmit="return false">
                                    <!-- Informasi Surat -->
                                    <div id="details" class="content pt-3 pt-lg-0">
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="number" id="nomor_surat" name="nomor_surat"
                                                    class="form-control" placeholder="01992023" />
                                                <label for="nomor_surat">Nomor Surat</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <select id="objek_pengawasan" class="form-select">
                                                    <option>Objek Pengawasan</option>
                                                    <option value="1">SPBU</option>
                                                    <option value="2">Alkohol</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text" id="tentang" name="tentang" class="form-control"
                                                    placeholder="Masukkan rincian mengenai surat tugas yang akan dibuat." />
                                                <label for="tentang">Tentang</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="input-group input-group-merge">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="date" id="html5-date-input" name="tanggal_surat"
                                                        class="form-control" placeholder="20-08-2023" />
                                                    <label for="tanggal_surat">Tanggal Surat</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="date" id="html5-date-input" name="tanggal_pengawasan"
                                                    class="form-control" placeholder="28-08-2023" />
                                                <label for="tanggal_pengawasan">Tanggal Pengawasan</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text" id="sarana" name="sarana" class="form-control"
                                                    placeholder="Mobil Innova 1 Unit" />
                                                <label for="sarana">Sarana</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="file" id="berkas_surat_tugas" name="berkas_surat_tugas"
                                                    class="form-control" placeholder="Upload Berkas Surat Tugas" />
                                                <label for="berkas_surat_tugas">Berkas Surat Tugas</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text" id="nama_perusahaan" name="nama_perusahaan"
                                                    class="form-control" placeholder="PT. AJI KUSUMA SPBU" />
                                                <label for="nama_perusahaan">Nama Perusahaan</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text" id="alamat" name="alamat" class="form-control"
                                                    placeholder="Jalan Ir. Juanda No 12, Jakarta" />
                                                <label for="alamat">Alamat</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text" id="nama_pengelola" name="nama_pengelola"
                                                    class="form-control" placeholder="AJI KUSUMA" />
                                                <label for="nama_pengelola">Nama Pengelola</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="input-group input-group-merge">
                                                <span class="input-group-text">ID (+62)</span>
                                                <div class="form-floating form-floating-outline">
                                                    <input type="phone" id="nomor_handphone" name="nomor_handphone"
                                                        class="form-control phone-number-mask"
                                                        placeholder="0811 2233 3445" />
                                                    <label for="nomor_handphone">Nomor Handphone</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex justify-content-between mt-4">
                                            <button class="btn btn-outline-secondary btn-prev" disabled>
                                                <i class="mdi mdi-arrow-left me-2"></i>
                                                <span class="align-middle">Previous</span>
                                            </button>
                                            <button class="btn btn-primary btn-next">
                                                <span class="align-middle me-sm-1">Next</span> <i
                                                    class="mdi mdi-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <!-- Petugas -->
                                    <div id="frameworks" class="content pt-3 pt-lg-0">
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="number" id="nomor_nip" name="nomor_nip"
                                                    class="form-control" placeholder="11923" />
                                                <label for="nomor_nip">Nomor NIP</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text" id="nama_petugas" name="nama_petugas" class="form-control"
                                                    placeholder="Masukkan Nama Petugas." />
                                                <label for="nama_petugas">Nama Petugas</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <select id="pilih_jabatan" class="form-select">
                                                    <option>Jabatan</option>
                                                    <option value="1">Sub Koordinator Bidang Industri</option>
                                                    <option value="2">Sub Koordinator Bidang Perdagangan</option>
                                                    <option value="3">Staff Pengawasan Bidang Industri</option>
                                                    <option value="4">Staff Pengawasan Bidang Perdagangan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <select id="unit_kerja" class="form-select">
                                                    <option>Unit Kerja</option>
                                                    <option value="1">Kepala Bidang Pengawasan</option>
                                                    <option value="2">Staff Bidang Pengawasan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text" id="golongan" name="golongan" class="form-control" placeholder="Contoh: III A" />
                                                <label for="golongan">Golongan</label>
                                            </div>
                                        </div>
                                        <div class="col-12 p-2">
                                            <div class="input-group input-group-merge">
                                                <span class="input-group-text">ID (+62)</span>
                                                <div class="form-floating form-floating-outline">
                                                    <input type="phone" id="nomor_handphone" name="nomor_handphone"
                                                        class="form-control phone-number-mask"
                                                        placeholder="0811 2233 3445" />
                                                    <label for="nomor_handphone">Nomor Handphone</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex justify-content-between mt-4">
                                            <button class="btn btn-outline-secondary btn-prev">
                                                <i class="mdi mdi-arrow-left me-2"></i> <span
                                                    class="align-middle">Previous</span>
                                            </button>
                                            <button class="btn btn-primary btn-next">
                                                <span class="align-middle me-sm-1">Next</span> <i
                                                    class="mdi mdi-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <!-- submit -->
                                    <div id="submit" class="content text-center pt-3 pt-lg-0">
                                        <h4 class="mb-2 mt-3">Submit</h4>
                                        <p>Tekan Submit untuk memproses surat tugas.</p>
                                        <!-- image -->
                                        <img src="{{ URL::asset('public/admin/assets/img/illustrations/create-app-modal-illustration-light.png') }}"
                                            alt="Create App img" width="265" class="img-fluid"
                                            data-app-light-img="illustrations/create-app-modal-illustration-light.png"
                                            data-app-dark-img="illustrations/create-app-modal-illustration-dark.png" />
                                        <div class="col-12 d-flex justify-content-between mt-4 pt-2">
                                            <button class="btn btn-outline-secondary btn-prev">
                                                <i class="mdi mdi-arrow-left me-2"></i> <span
                                                    class="align-middle">Previous</span>
                                            </button>
                                            <button class="btn btn-success btn-next btn-submit">
                                                <span class="align-middle">Submit</span><i class="mdi mdi-check ms-2"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--/ Property Listing Wizard -->
                </div>
            </div>
        </div>
        <!--/ Create App Modal -->


    </div>

    @push('custom-scripts')
        <script src="{{ URL::asset('resources/js/master/surat-tugas-datatable.js') }}"></script>
        <script src="{{ URL::asset('public/admin/assets/js/modal-create-app.js') }}"></script>
    @endpush
@endsection
