@extends('layouts.admin._master-admin')
@section('content')

<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Master /</span> Alkohol</h4>

    <!-- DataTable with Buttons -->
    <div class="card">
        <div class="card-datatable table-responsive pt-0">
            <table class="datatables-admin table table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Date</th>
                        <th>Salary</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- Modal to add new record -->
    <div class="offcanvas offcanvas-end" id="add-new-record">
        <div class="offcanvas-header border-bottom">
            <h5 class="offcanvas-title" id="exampleModalLabel">Tambah Admin</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body flex-grow-1">
            <form action="" class="add-new-record pt-0 row g-3" id="form-add-new-record" method="post">
                <div class="col-sm-12">
                    <div class="input-group input-group-merge">
                        <span id="basicPost2" class="input-group-text"><i class="mdi mdi-briefcase-outline"></i></span>
                        <div class="form-floating form-floating-outline">
                            <input type="number" name="nrk" id="basicPost" class="form-control dt-post" placeholder="129121" aria-label="129121" aria-describedby="basicPost2" />
                            <label for="basicPost">NRK</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group input-group-merge">
                        <span id="basicFullname2" class="input-group-text">
                            <i class="mdi mdi-account-outline"></i>
                        </span>
                        <div class="form-floating form-floating-outline">
                            <input type="text" name="full_name" id="basicFullname" class="form-control dt-full-name" placeholder="Didin Maulana Aksan" aria-label="John Doe" aria-describedby="basicFullname2" />
                            <label for="basicFullname">Nama Lengkap</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group input-group-merge">
                        <span id="basicPost2" class="input-group-text"><i class="mdi mdi-briefcase-outline"></i></span>
                        <div class="form-floating form-floating-outline">
                            <input type="number" name="no_hp" id="basicPost" class="form-control dt-post" placeholder="813873000000" aria-label="813873000000" aria-describedby="basicPost2" />
                            <label for="basicPost">No Hp</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group input-group-merge">
                        <span class="input-group-text"><i class="mdi mdi-email-outline"></i></span>
                        <div class="form-floating form-floating-outline">
                            <input type="text" name="email" id="basicEmail" class="form-control dt-email" placeholder="john.doe@example.com" aria-label="john.doe@example.com" />
                            <label for="basicEmail">Email</label>
                        </div>
                    </div>
                    <div class="form-text">Gunakan Email Aktif untuk menerima password..</div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group input-group-merge">
                        <span id="basicNrk2" class="input-group-text">
                            <i class="mdi mdi-account-outline"></i>
                        </span>
                        <div class="form-floating form-floating-outline">
                            <input type="number" name="nrk" class="form-control dt-date" id="basicNrk" aria-describedby="basicNrk2" placeholder="139221" aria-label="139221" />
                            <label for="basicNrk"></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group input-group-merge">
                        <span id="basicSalary2" class="input-group-text"><i class="mdi mdi-currency-usd"></i></span>
                        <div class="form-floating form-floating-outline">
                            <select class="form-control" name="role">

                            </select>
                            {{-- <input type="number" id="basicSalary" name="basicSalary" class="form-control dt-salary" placeholder="12000" aria-label="12000" aria-describedby="basicSalary2" />
                            <label for="basicSalary">Role</label> --}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary data-submit me-sm-3 me-1">Submit</button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="offcanvas">Cancel</button>
                </div>
            </form>
        </div>
    </div>
    <!--/ DataTable with Buttons -->

</div>

@push('custom-scripts')
    <script src="{{ URL::asset('resources/js/master/admin-datatable.js') }}"></script>
@endpush

@endsection
