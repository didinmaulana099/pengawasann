@extends('layouts.admin._master-admin')
@section('content')

    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Master /</span> SPBU </h4>

        <!-- DataTable with Buttons -->
        <div class="card">
            <div class="card-datatable table-responsive pt-0">
                <table class="datatables-spbu table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Pengelola</th>
                            <th>No HP</th>
                            <th>NIB</th>
                            <th>No. UTTP</th>
                            <th>No. Surat Pertamina</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--/ DataTable with Buttons -->

        <!-- Edit User Modal -->
        <div class="modal fade" id="addSpbuModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-simple modal-edit-user">
                <div class="modal-content p-3 p-md-5">
                    <div class="modal-body py-3 py-md-0">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        <div class="text-center mb-4">
                            <h3 class="mb-2">Form Tambah Data SPBU</h3>
                            <p class="pt-1">Silahkan lengkapi kebutuhan data dibawah ini.</p>
                        </div>
                        <form id="editSpbuForm" class="row g-4" onsubmit="return false">
                            <div class="col-12 col-md-6">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="nama_perusahaan" name="nama_perusahaan"
                                        class="form-control" placeholder="PT. AJI KUSUMA SPBU" />
                                    <label for="nama_perusahaan">Nama Perusahaan</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="nomor_spbu" name="nomor_spbu"
                                        class="form-control" placeholder="123232" />
                                    <label for="nomor_spbu">Nomor SPBU</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="nama_pengelola" name="nama_pengelola"
                                        class="form-control" placeholder="Masukkan nama lengkap anda" />
                                    <label for="nama_pengelola">Nama Pengelola</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="input-group input-group-merge">
                                    <span class="input-group-text">ID (+62)</span>
                                    <div class="form-floating form-floating-outline">
                                        <input type="text" id="nomor_handphone" name="nomor_handphone"
                                            class="form-control phone-number-mask" placeholder="0811 2233 3445" />
                                        <label for="nomor_handphone">Nomor Handphone</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="email_perusahaan" name="email_perusahaan"
                                        class="form-control" placeholder="example@gmail.com" />
                                    <label for="email_perusahaan">Email</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="alamat_lengkap" name="alamat_lengkap"
                                        class="form-control" placeholder="Jalan Ir. Juanda Nomor 12, Jakarta" />
                                    <label for="alamat_lengkap">Alamat Lengkap</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="nomor_nib" name="nomor_nib"
                                        class="form-control" placeholder="0009212" />
                                    <label for="nomor_nib">Nomor NIB</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="nomor_uttp" name="nomor_uttp"
                                        class="form-control" placeholder="992123" />
                                    <label for="nomor_uttp">Nomor UTTP</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="nama_pertamina" name="nama_pertamina"
                                        class="form-control" placeholder="PERTAMINA AJI KUSUMA" />
                                    <label for="nama_pertamina">Nama Pertamina</label>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Edit User Modal -->

    </div>

    @push('custom-scripts')
        <script src="{{ URL::asset('resources/js/master/spbu-datatable.js') }}"></script>
    @endpush
@endsection
