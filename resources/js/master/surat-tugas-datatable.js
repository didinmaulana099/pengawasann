/**
 * App user list
 */

"use strict";

// Datatable (jquery)
$(function () {
    var dataTableSurat = $(".datatables-surat-tugas"),
        dt_surat;

    // Users List datatable

    if (dataTableSurat.length) {
        dt_surat = dataTableSurat.DataTable({
            ajax: {
                url: `/get-data/surat-tugas`,
            },
            columns: [
                // columns according to JSON
                // { data: '1' },
                { data: "" },
                { data: "perusahaan"},
                { data: "nomor_spbu"},
                { data: "nama_pengelola" },
                { data: "nomor_hp" },
                { data: "nomor_nib" },
                { data: "nomor_sertifikat_uttp" },
                { data: "nomor_surat_pertamina" },
                { data: "sisa_hari" },
                { data: "status" },
                { data: "" },
            ],
            columnDefs: [
                {
                    // For Responsive
                    className: "center",
                    orderable: true,
                    searchable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    // Actions
                    targets: -1,
                    searchable: false,
                    title: "Actions",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return (
                            '<span class="text-nowrap"><button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2" data-bs-target="#createApp" data-bs-toggle="modal" data-bs-dismiss="modal""><span class="mdi mdi-plus-circle-outline mdi-20px"></span></button>' +
                            '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2" data-bs-target="#editSpbuModal" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>' +
                            '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon delete-record"><i class="mdi mdi-delete-outline mdi-20px"></i></button></span>'
                        );
                    },
                },
            ],
            order: [[0, "asc"]],
            dom:
                '<"row mx-1"' +
                '<"col-sm-12 col-md-3" l>' +
                // '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
                ">t",
            language: {
                sLengthMenu: "Show _MENU_",
                search: "Search",
                searchPlaceholder: "Search..",
            },
            // Buttons with Dropdown
            // buttons: [
            //     {
            //         text: "EDIT SPBU",
            //         className: "add-new btn btn-primary mb-3 mb-md-0",
            //         attr: {
            //             "data-bs-toggle": "modal",
            //             "data-bs-target": "#editSpbuModal",
            //         },
            //         init: function (api, node, config) {
            //             $(node).removeClass("btn-secondary");
            //         },
            //     },
            // ],
            // For responsive popup
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Details of " + data["nama"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                      col.rowIndex +
                                      '" data-dt-column="' +
                                      col.columnIndex +
                                      '">' +
                                      "<td>" +
                                      col.title +
                                      ":" +
                                      "</td> " +
                                      "<td>" +
                                      col.data +
                                      "</td>" +
                                      "</tr>"
                                : "";
                        }).join("");

                        return data
                            ? $('<table class="table"/><tbody />').append(data)
                            : false;
                    },
                },
            },
            initComplete: function () {
                // Adding role filter once table initialized
                this.api()
                    .columns(3)
                    .every(function () {
                        var column = this;
                        var select = $(
                            '<select id="UserRole" class="form-select text-capitalize"><option value=""> Select Role </option></select>'
                        )
                            .appendTo(".user_role")
                            .on("change", function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search(
                                        val ? "^" + val + "$" : "",
                                        true,
                                        false
                                    )
                                    .draw();
                            });

                        column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append(
                                    '<option value="' +
                                        d +
                                        '" class="text-capitalize">' +
                                        d +
                                        "</option>"
                                );
                            });
                    });
            },
        });

        // Delete Record
        $('.datatables-spbu tbody').on('click', '.delete-record', function () {
            dt_basic.row($(this).parents('tr')).remove().draw();
        });
    }

    // if (dataTableSpbu.length) {
    //     dt_spbu = dataTableSpbu.DataTable({
    //         ajax: {
    //             url: `/get-data/spbu`,
    //         },
    //         columns: [
    //             // columns according to JSON
    //             // { data: '1' },
    //             { data: "" },
    //             { data: "kode_spbu"},
    //             { data: "nama_spbu"},
    //             { data: "alamat" },
    //             { data: "nomor_hp" },
    //             { data: "nomor_nib" },
    //             { data: "tanggal_nib" },
    //             { data: "nomor_sertifikat_uttp" },
    //             { data: "tanggal_sertifikat_uttp" },
    //             { data: "nomor_surat_pertamina" },
    //             { data: "tanggal_surat_pertamina" },
    //             { data: "created_by" },
    //             { data: "changed_by" },
    //             { data: "deleted_by" },
    //             { data: "" },
    //         ],
    //         columnDefs: [
    //             {
    //                 // For Responsive
    //                 className: "center",
    //                 orderable: true,
    //                 searchable: false,
    //                 responsivePriority: 2,
    //                 targets: 0,
    //                 render: function (data, type, row, meta) {
    //                     return meta.row + meta.settings._iDisplayStart + 1;
    //                 },
    //             },
    //             {
    //                 // Actions
    //                 targets: -1,
    //                 searchable: false,
    //                 title: "Actions",
    //                 orderable: false,
    //                 render: function (data, type, full, meta) {
    //                     return (
    //                         '<span class="text-nowrap"><button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2" data-bs-target="#editRoleModal" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>' +
    //                         '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon delete-record"><i class="mdi mdi-delete-outline mdi-20px"></i></button></span>'
    //                     );
    //                 },
    //             },
    //         ],
    //         order: [[0, "asc"]],
    //         dom:
    //             '<"row mx-1"' +
    //             '<"col-sm-12 col-md-3" l>' +
    //             '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
    //             ">t" +
    //             '<"row mx-2"' +
    //             '<"col-sm-12 col-md-6"i>' +
    //             '<"col-sm-12 col-md-6"p>' +
    //             ">",
    //         language: {
    //             sLengthMenu: "Show _MENU_",
    //             search: "Search",
    //             searchPlaceholder: "Search..",
    //         },
    //         // Buttons with Dropdown
    //         buttons: [
    //             {
    //                 text: "SYNQ SIKEMET",
    //                 className: "add-new btn btn-primary mb-3 mb-md-0",
    //                 attr: {
    //                     "data-bs-toggle": "modal",
    //                     "data-bs-target": "#addRoleModal",
    //                 },
    //                 init: function (api, node, config) {
    //                     $(node).removeClass("btn-secondary");
    //                 },
    //             },
    //         ],
    //         // For responsive popup
    //         responsive: {
    //             details: {
    //                 display: $.fn.dataTable.Responsive.display.modal({
    //                     header: function (row) {
    //                         var data = row.data();
    //                         return "Details of " + data["nama"];
    //                     },
    //                 }),
    //                 type: "column",
    //                 renderer: function (api, rowIdx, columns) {
    //                     var data = $.map(columns, function (col, i) {
    //                         return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
    //                             ? '<tr data-dt-row="' +
    //                                   col.rowIndex +
    //                                   '" data-dt-column="' +
    //                                   col.columnIndex +
    //                                   '">' +
    //                                   "<td>" +
    //                                   col.title +
    //                                   ":" +
    //                                   "</td> " +
    //                                   "<td>" +
    //                                   col.data +
    //                                   "</td>" +
    //                                   "</tr>"
    //                             : "";
    //                     }).join("");

    //                     return data
    //                         ? $('<table class="table"/><tbody />').append(data)
    //                         : false;
    //                 },
    //             },
    //         },
    //         initComplete: function () {
    //             // Adding role filter once table initialized
    //             this.api()
    //                 .columns(3)
    //                 .every(function () {
    //                     var column = this;
    //                     var select = $(
    //                         '<select id="UserRole" class="form-select text-capitalize"><option value=""> Select Role </option></select>'
    //                     )
    //                         .appendTo(".user_role")
    //                         .on("change", function () {
    //                             var val = $.fn.dataTable.util.escapeRegex(
    //                                 $(this).val()
    //                             );
    //                             column
    //                                 .search(
    //                                     val ? "^" + val + "$" : "",
    //                                     true,
    //                                     false
    //                                 )
    //                                 .draw();
    //                         });

    //                     column
    //                         .data()
    //                         .unique()
    //                         .sort()
    //                         .each(function (d, j) {
    //                             select.append(
    //                                 '<option value="' +
    //                                     d +
    //                                     '" class="text-capitalize">' +
    //                                     d +
    //                                     "</option>"
    //                             );
    //                         });
    //                 });
    //         },
    //     });
    // }
});
