/**
 * App user list
 */

"use strict";

// Datatable (jquery)
$(function () {
    var dataTableRole = $(".datatables-role"),
        dt_role;

    // Users List datatable

    if (dataTableRole.length) {
        dt_role = dataTableRole.DataTable({
            ajax: {
                url: `/role/get-role`,
            },
            columns: [
                // columns according to JSON
                // { data: '1' },
                { data: "" },
                { data: "nama" },
                { data: "keterangan" },
                { data: "created_by" },
                { data: "changed_by" },
                { data: "" },
            ],
            columnDefs: [
                {
                    // For Responsive
                    className: "center",
                    orderable: true,
                    searchable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    // Name
                    targets: 2,
                    render: function (data, type, full, meta) {
                        var $keterangan = full["keterangan"];
                        return (
                            '<p href="' +
                            '" class="text-nowrap" style"color:teal">' +
                            $keterangan +
                            "</p>"
                        );
                    },
                },
                {
                    // For Responsive
                    className: "align-top",
                    orderable: true,
                    searchable: false,
                    responsivePriority: 2,
                    targets: 3,
                    render: function (data, type, full, meta) {
                        var $created_by = full["created_by"];
                        return (
                            '<p href="' +
                            '" class="text-nowrap" style"color:teal">' +
                            $created_by +
                            "</p>"
                        );
                    },
                },
                {
                    // For Responsive
                    className: "align-top",
                    orderable: true,
                    searchable: false,
                    responsivePriority: 2,
                    targets: 4,
                    render: function (data, type, full, meta) {
                        var $updated_by = full["changed_by"];
                        return (
                            '<p href="' +
                            '" class="text-nowrap" style"color:teal">' +
                            $updated_by +
                            "</p>"
                        );
                    },
                },
                {
                    // Actions
                    targets: -1,
                    searchable: false,
                    title: "Actions",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return (
                            '<span class="text-nowrap"><button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2" data-bs-target="#editRoleModal" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>' +
                            '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon delete-record"><i class="mdi mdi-delete-outline mdi-20px"></i></button></span>'
                        );
                    },
                },
            ],
            order: [[0, "asc"]],
            dom:
                '<"row mx-1"' +
                '<"col-sm-12 col-md-3" l>' +
                '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
                ">t" +
                '<"row mx-2"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                ">",
            language: {
                sLengthMenu: "Show _MENU_",
                search: "Search",
                searchPlaceholder: "Search..",
            },
            // Buttons with Dropdown
            buttons: [
                {
                    text: "TAMBAH ROLE",
                    className: "add-new btn btn-primary mb-3 mb-md-0",
                    attr: {
                        "data-bs-toggle": "modal",
                        "data-bs-target": "#addRoleModal",
                    },
                    init: function (api, node, config) {
                        $(node).removeClass("btn-secondary");
                    },
                },
            ],
            // For responsive popup
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Details of " + data["nama"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                      col.rowIndex +
                                      '" data-dt-column="' +
                                      col.columnIndex +
                                      '">' +
                                      "<td>" +
                                      col.title +
                                      ":" +
                                      "</td> " +
                                      "<td>" +
                                      col.data +
                                      "</td>" +
                                      "</tr>"
                                : "";
                        }).join("");

                        return data
                            ? $('<table class="table"/><tbody />').append(data)
                            : false;
                    },
                },
            },
            initComplete: function () {
                // Adding role filter once table initialized
                this.api()
                    .columns(3)
                    .every(function () {
                        var column = this;
                        var select = $(
                            '<select id="UserRole" class="form-select text-capitalize"><option value=""> Select Role </option></select>'
                        )
                            .appendTo(".user_role")
                            .on("change", function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search(
                                        val ? "^" + val + "$" : "",
                                        true,
                                        false
                                    )
                                    .draw();
                            });

                        column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append(
                                    '<option value="' +
                                        d +
                                        '" class="text-capitalize">' +
                                        d +
                                        "</option>"
                                );
                            });
                    });
            },
        });
    }
});

(function () {
    // On edit role click, update text
    var roleEditList = document.querySelectorAll(".role-edit-modal"),
        roleAdd = document.querySelector(".add-new-role"),
        roleTitle = document.querySelector(".role-title");

    roleAdd.onclick = function () {
        roleTitle.innerHTML = "Add New Role"; // reset text
    };
    if (roleEditList) {
        roleEditList.forEach(function (roleEditEl) {
            roleEditEl.onclick = function () {
                roleTitle.innerHTML = "Edit Role"; // reset text
            };
        });
    }
})();
