<?php

use Illuminate\Support\Facades\Route;

//admin
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\ConfigurationController;
use App\Http\Controllers\Admin\MAdminController;
use App\Http\Controllers\Admin\DashboardAdminController;
use App\Http\Controllers\Admin\MSpbuController;
use App\Http\Controllers\Admin\MSuratTugasController;
use App\Http\Controllers\Admin\MSaranaController;
use App\Http\Controllers\Admin\MAlkoholController;
use App\Http\Controllers\Admin\MBeritaAcaraController;
use App\Http\Controllers\Admin\MKlarifikasiController;
use App\Http\Controllers\Admin\MSegelController;
use App\Http\Controllers\Admin\MPermohonanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// admin route
Route::get('/', [AuthController::class, 'index'])->name('auth.login');
Route::get('/login', [AuthController::class, 'index'])->name('auth.login');

Route::middleware(['has_login'])->group(function () {
    // Dashboard Admin
    Route::get('/dashboard-admin', [DashboardAdminController::class, 'index'])->name('admin.dashboard');

    // master Data
    Route::get('/master-admin', [MAdminController::class, 'indexAdmin'])->name('admin.master-admin.index');
    Route::get('/master-admin/get-data', [MAdminController::class, 'getDataAdmin'])->name('admin.master-admin.get-data');
    Route::post('/master-admin/store', [MAdminController::class, 'storeAdmin'])->name('admin.master-admin.store');

    Route::get('/admin/pengguna', [MAdminController::class, 'indexPengguna'])->name('admin.pengguna');

    Route::get('/admin/spbu', [MSpbuController::class, 'index'])->name('admin.spbu');
    Route::get('/get-data/spbu', [MSpbuController::class, 'getData'])->name('admin.spbu.get-data');

    Route::get('/admin/surat-tugas', [MSuratTugasController::class, 'index'])->name('admin.surat-tugas');
    Route::get('/get-data/surat-tugas', [MSuratTugasController::class, 'getData'])->name('admin.surat-tugas.get-data');

    Route::get('/admin/sarana', [MSaranaController::class, 'index'])->name('admin.sarana');

    Route::get('/admin/alkohol', [MAlkoholController::class, 'index'])->name('admin.alkohol');
    
    Route::get('/admin/berita-acara', [MBeritaAcaraController::class, 'index'])->name('admin.berita-acara');
    
    Route::get('/admin/klarifikasi', [MKlarifikasiController::class, 'index'])->name('admin.klarifikasi');
    
    Route::get('/admin/segel', [MSegelController::class, 'index'])->name('admin.segel');
    
    Route::get('/admin/permohonan', [MPermohonanController::class, 'index'])->name('admin.permohonan');

    // Settings Profile
    Route::get('/user-profile', [ProfileController::class, 'index'])->name('admin.profile.index');

    //general route
    Route::get('/configuration', [ConfigurationController::class, 'index'])->name('admin.configuration.index');

    // Permission
    Route::get('/permission/get-data', [PermissionController::class, 'getData'])->name('admin.permission.get-data');
    Route::get('/permission', [PermissionController::class, 'index'])->name('admin.permission.index');
    Route::post('/permission/store', [PermissionController::class, 'store'])->name('admin.permission.store');
    
    // role
    Route::get('/role/get-role', [RoleController::class, 'getData'])->name('admin.role.get-data');
    Route::get('/role', [RoleController::class, 'index'])->name('admin.role.index');
    Route::post('/role-store', [RoleController::class, 'store'])->name('admin.role.store');

    // Logout
    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');
});

