<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCMinumanAlkoholStokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_minuman_alkohol_stok', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_berita_acara');
            $table->string('jenis_minuman_alkohol');
            $table->integer('qty');
            $table->string('satuan');
            $table->string('jenis_stok');
            $table->string('created_by')->nullable();
            $table->string('changed_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_minuman_alkohol_stok');
    }
}
