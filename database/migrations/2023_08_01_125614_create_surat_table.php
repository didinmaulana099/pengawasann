<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor_surat');
            $table->date('tanggal_surat');
            $table->string('objek_pengawasan');
            $table->date('tanggal_pelaksanaan');
            $table->string('perihal');
            $table->string('jenis_surat');
            $table->string('url')->nullable();
            $table->integer('NIP');
            $table->integer('kode_perusahaan');
            $table->string('kode_sarana');
            $table->string('created_by')->nullable();
            $table->string('changed_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**s
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat');
    }
}
