<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMPerusahaanMinumanAlkoholTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_perusahaan_minuman_alkohol', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('kode_perusahaan_minuman_alkohol');
            $table->string('nama_perusahaan_minuman_alkohol');
            $table->string('alamat')->nullable();
            $table->string('nama_pengelola')->nullable();
            $table->integer('nomor_hp')->nullable();
            $table->string('jenis_perusahaan')->nullable();       
            $table->string('created_by')->nullable();
            $table->string('changed_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_perusahaan_minuman_alkohol');
    }
}
