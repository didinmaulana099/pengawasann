<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCSpbuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_spbu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('merk_pompa');
            $table->string('tipe');
            $table->string('nomor_seri');
            $table->string('nomor_nozel');
            $table->string('media')->nullable();
            $table->string('hasil')->nullable();
            $table->string('keterangan')->nullable();
            $table->integer('id_berita_acara');
            $table->string('created_by')->nullable();
            $table->string('changed_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_spbu');
    }
}
