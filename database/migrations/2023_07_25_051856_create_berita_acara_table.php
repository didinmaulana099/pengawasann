<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeritaAcaraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berita_acara', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor_surat');
            $table->integer('kode_perusahaan');
            $table->integer('NIP');
            $table->string('id_berita_acara');
            $table->string('hasil_pelaksanaan');
            $table->string('kesimpulan')->nullable();
            $table->string('jenis_berita_acara');
            $table->string('jenis_surat');
            $table->string('jenis_objek_pengawasan');
            $table->string('url')->nullable();
            $table->string('photo')->nullable();
            $table->string('created_by')->nullable();
            $table->string('changed_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berita_acara');
    }
}
