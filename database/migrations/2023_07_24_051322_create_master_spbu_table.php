<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterSpbuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_spbu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_spbu');
            $table->string('nama_spbu');
            $table->string('alamat')->nullable();
            $table->string('nama_pengelola')->nullable();
            $table->integer('nomor_hp')->nullable();
            $table->integer('nomor_nib')->nullable();
            $table->string('tanggal_nib')->nullable();
            $table->integer('nomor_sertifikat_uttp')->nullable();
            $table->date('tanggal_sertifikat_uttp')->nullable();
            $table->integer('nomor_surat_pertamina')->nullable();
            $table->date('tanggal_surat_pertamina')->nullable();
            $table->string('created_by')->nullable();
            $table->string('changed_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_spbu');
    }
}
