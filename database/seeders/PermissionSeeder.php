<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Permission::truncate();

        $permissions = [
            [
                "nama" => "Dashboard",
                "keterangan" => null,
                "url" => "admin.dashboard",
                "url_path" => "dashboard-admin",
                "icon" => "menu-icon tf-icons mdi mdi-home-outline",
                "order_number" => 1,
                "created_by" => "seeder",
                "changed_by" => "seeder",
                "children" => [
                    [
                        "nama" => "Dashboard Admin",
                        "keterangan" => null,
                        "url" => "admin.dashboard",
                        "url_path" => "dashboard-admin",
                        "icon" => null,
                        "order_number" => 1,
                        "created_by" => "seeder",
                        "changed_by" => "seeder",
                    ]
                ]
            ],
            [
                "nama" => "Master Data",
                "keterangan" => null,
                "url" => "master-data",
                "url_path" => "master-data",
                "icon" => "menu-icon tf-icons mdi mdi-window-maximize",
                "order_number" => 2,
                "created_by" => "seeder",
                "changed_by" => "seeder",
                "children" => [
                    [
                        "nama" => "Admin",
                        "keterangan" => null,
                        "url" => "admin.master-admin.index",
                        "url_path" => "master-admin",
                        "icon" => null,
                        "order_number" => 1,
                        "created_by" => "seeder",
                        "changed_by" => "seeder",
                    ],
                    [
                        "nama" => "Pengguna",
                        "keterangan" => null,
                        "url" => "admin.pengguna",
                        "url_path" => "admin-pengguna",
                        "icon" => null,
                        "order_number" => 2,
                        "created_by" => "seeder",
                        "changed_by" => "seeder",
                    ]
                ]
            ],
            [
                "nama" => "Pengaturan",
                "keterangan" => null,
                "url" => "master.setting",
                "url_path" => "master-setting",
                "icon" => "mdi mdi-cog-outline me-2",
                "order_number" => 3,
                "created_by" => "seeder",
                "changed_by" => "seeder",
                "children" => [
                    [
                        "nama" => "Konfigurasi",
                        "keterangan" => null,
                        "url" => "admin.configuration.index",
                        "url_path" => "configuration",
                        "icon" => null,
                        "order_number" => 1,
                        "created_by" => "seeder",
                        "changed_by" => "seeder",
                    ],
                    [
                        "nama" => "Permission",
                        "keterangan" => null,
                        "url" => "admin.permission.index",
                        "url_path" => "permission",
                        "icon" => null,
                        "order_number" => 2,
                        "created_by" => "seeder",
                        "changed_by" => "seeder",
                    ],
                    [
                        "nama" => "Role",
                        "keterangan" => null,
                        "url" => "admin.role.index",
                        "url_path" => "role",
                        "icon" => null,
                        "order_number" => 3,
                        "created_by" => "seeder",
                        "changed_by" => "seeder",
                    ]
                ]
            ],
        ];

        foreach (collect($permissions) as $permission) {
            $data = new Permission;
            $data->nama = $permission['nama'];
            $data->keterangan = $permission['keterangan'];
            $data->url = $permission['url'];
            $data->url_path = $permission['url_path'];
            $data->icon = $permission['icon'];
            $data->parent_id = null;
            $data->parent_nama = null;
            $data->order_number = $permission['order_number'];
            $data->created_by = $permission['created_by'];
            $data->changed_by = $permission['changed_by'];
            $data->save();

            if (!empty($permission['children'])) {
                foreach ($permission['children'] as $child) {
                    $data2 = new Permission;
                    $data2->nama = $child['nama'];
                    $data2->keterangan = $child['keterangan'];
                    $data2->url = $child['url'];
                    $data2->url_path = $child['url_path'];
                    $data2->icon = $child['icon'];
                    $data2->parent_id = $data->id;
                    $data2->parent_nama = $data->nama;
                    $data2->order_number = $data->order_number;
                    $data2->created_by = $data->created_by;
                    $data2->changed_by = $data->changed_by;
                    $data2->save();
                }
            }
        }
    }
}
