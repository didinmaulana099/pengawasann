<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'username' => 'superadmin',
                'password' => Hash::make('09910991'),
                'nama_lengkap' => 'Super Admmin',
                'api_token' => '',
                'active' => 1,
                'created_by' => 'admin',
                'changed_by' => 'admin'
            ],
            [
                'username' => 'admin',
                'password' => Hash::make('09910991'),
                'nama_lengkap' => 'Admin',
                'api_token' => '',
                'active' => 1,
                'created_by' => 'admin',
                'changed_by' => 'admin'
            ]
        ];

        $role = Role::first();

        foreach ($datas as $data) {

            $user = User::firstOrNew(['username' => $data['username']]);
            $user->username = $data['username'];
            $user->password = $data['password'];
            $user->nama_lengkap = $data['nama_lengkap'];
            $user->api_token = null;
            $user->active = $data['active'];
            $user->created_by = $data['created_by'];
            $user->changed_by = $data['changed_by'];
            $user->role_id = $role->id;
            $user->role_nama = $role->nama;
            $user->save();

        }
    }
}
