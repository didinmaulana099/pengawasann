<?php

namespace App\Http\Controllers\Helpers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;

class MainSett extends Controller
{
    public static function getUserDataSession()
    {
        $dataUser = Session::get('data_user');
        return $dataUser;
    }
}
