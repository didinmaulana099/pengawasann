<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permission;

class PermissionController extends Controller
{
    public function getData(){
        $permission = Permission::with('details')->get();

        return response()->json([
            'type' => 'success',
            'data' => $permission
        ], 200);
    }

    public function index() {
        $listPermission = Permission::where('parent_id', null)->get();
        return view('admin.settings.permission.index', [
            'list_permission' => $listPermission,
        ]);
    }

    public function store(Request $request)
    {
        $permission = Permission::where('parent_id', $request->parent_id)->first();
        $orderNumber = $permission->order_number;
        $parent = Permission::find($request->parent_id);
        $permission = new Permission;
        $permission->nama = $request->nama_permssion;
        $permission->keterangan = $request->keterangan;
        $permission->url = $request->url;
        $permission->url_path = $request->url_path;
        $permission->icon = $request->icon;
        $permission->parent_id = $request->parent_id;
        $permission->parent_nama = $parent->nama;
        $permission->order_number = $orderNumber;
        $permission->created_by = auth()->user()->created_by;
        $permission->changed_by = auth()->user()->changed_by;
        $permission->save();

        return response()->json([
            'type' => 'success',
            'message' => 'Permission Berhasil Ditambahkan!',
        ], 200);

        // return redirect()->route('admin.permission.index');
    }

    public function show(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);

        return response()->json([
            'type' => 'success',
            'data' => $permission
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $permission = Permission::findOrFail($id);

        $permission->name = $request->name;
        $permission->description = $request->description;
        $permission->url = $request->url;
        $permission->icon = $request->icon;
        $permission->parent_id = $request->parent_id;
        $permission->parent_name = $request->parent_name;
        $permission->order_number = $request->order_number;
        $permission->changed_by = auth()->user()->changed_by;
        $permission->save();

        return redirect()->route('');
    }

    public function destroy(Request $request, $id)
    {
        $permission = Permission::where('_id', $id)->delete();
        return redirect()->route('');
    }

    public function list(Request $request)
    {
        $permissions = Permission::when($request->keyword, function($query) use ($request) {
            if (!empty($request->keyword)) {
                $query->where('name', 'like', '%'.$request->keyword.'%');
            }
        })->take(10)
        ->get();

        return response()->json([
            'type' => 'success',
            'data' => $permissions
        ], 200);
    }

    public function listParentId(Request $request)
    {
        $permissions = Permission::whereNull('parent_id')->get();

        return response()->json([
            'type' => 'success',
            'data' => $permissions
        ], 200);
    }

    public function get()
    {
        $permissions = Permission::all();

        return response()->json([
            'type' => 'success',
            'data' => $permissions
        ], 200);
    }
}

