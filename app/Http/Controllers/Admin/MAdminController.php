<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\ProfileAdmin;
use App\Models\Role;
use App\Models\User;

class MAdminController extends Controller
{
    public function indexAdmin(){
        $listRole = Role::get();
        return view('admin.master.m_admin', ['list_role' => $listRole]);
    }

    public function getDataAdmin(){
        $data = ProfileAdmin::orderBy('id', 'desc')->get();
        return response()->json([
            'data' => $data,
            'type' => 'success',
            'message' => 'get data berhasil',
        ], 200);
    }

    public function storeAdmin(Request $request){
        $role = Role::where('id', $request->role)->first();
        $admin = new User;
        $admin->nama_lengkap = $request->nama_lengkap;
        $admin->username = $request->nrk;
        $admin->password = Hash::make('09910991');
        $admin->role_id = $role->id;
        $admin->role_nama = $role->nama;
        $admin->active = 1;
        $admin->save();

        $profilAdmin = new ProfileAdmin;
        $profilAdmin->user_id = $admin->id;
        $profilAdmin->nama_lengkap = $request->nama_lengkap;
        $profilAdmin->nrk = $request->nrk;
        $profilAdmin->email = $request->email;
        $profilAdmin->no_hp = $request->no_hp;
        $profilAdmin->active = 1;
        // if (!empty($request->photo) && $request->photo != 'null') {

        //     $file = $request->file('photo');
        //     $file_extension = $file->extension();
        //     $filename = rand(0, 99).time().'.'.$file_extension;
        //     $file->storeAs('public/images', $filename);

        //     $profile->photo = $filename;
        // }
        $profilAdmin->save();

        return redirect()->route('admin.master-admin.index');
    }

    public function indexPengguna(){
        return view('admin.master.m_pengguna');
    }

}
