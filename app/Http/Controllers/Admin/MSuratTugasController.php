<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MSuratTugasController extends Controller
{
    public function index() {
        return view('admin.master.m_surat-tugas');
    }

    public function getData(){
        $data = [
            [
                "perusahaan" => "test",
                "nomor_spbu" => "test2",
                "nama_pengelola" => "test4",
                "nomor_hp" => "test",
                "nomor_nib" => "test",
                "nomor_sertifikat_uttp" => "test",
                "nomor_surat_pertamina" => "test",
                "sisa_hari" => "test",
                "status" => "test"
            ]
        ];

        return response()->json([
            'data' => $data,
            'type' => 'success',
        ], 200);
    }
}
