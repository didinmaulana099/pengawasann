<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Models\User;
use App\Models\Permission;
use Validator;
use Session;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'username' => 'required|exists:users',
            'password' => 'required|min:6'
        ]);

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = $user->createToken('token-name')->plainTextToken;

            $user->forceFill([
                'api_token' => $token
            ])->save();

            $permissions = Permission::whereNull('parent_id')->orderBy('order_number')->get();
            $permission_allowed = $permissions->map(function($permission) use ($user){
                $permission_allowed = collect($user->role->permissions)->where('visible', 1);
                if ($permission_allowed->pluck('permission_id')->contains($permission->id)) {
                    return [
                        '_id' => $permission->id,
                        'nama' => $permission->nama,
                        'url' => $permission->url,
                        'icon' => $permission->icon,
                        'children' => $permission->children->map(function($child) use ($user){
                            $permission_allowed = collect($user->role->permissions)->where('visible', 1);
                            if ($permission_allowed->pluck('permission_id')->contains($child->id)) {
                                return [
                                    '_id' => $child->id,
                                    'nama' => $child->nama,
                                    'url' => $child->url
                                ];
                            }
                        })
                        ->filter()
                        ->values()
                    ];
                }
            })
            ->filter()->values();

            $dataUser = [
                'token' => $token,
                'data' => $user,
                'permissions' => $permission_allowed->toArray(),
            ];

            $request->session()->put('data_user', $dataUser);

            return response()->json([
                'type' => 'success',
                'message' => 'Login successfully!',
                'token' => $token,
                'data' => $user,
                'permissions' => $permission_allowed->toArray(),
                'redirect' => Permission::find($user->role->permissions->where('visible', 1)->first()->permission_id)->url_path
            ], 200);

        }   else {
            return response()->json([
                'type' => 'error',
                'message' => 'Please check your email or password!',
                'errors' => [
                    'password' => [
                        'Your password is invalid!'
                    ]
                ]
            ], 422);
        }
    }

    public function logout(Request $request ) {
        Session::flush();
        Auth::logout();

        return redirect()->route('auth.login');
    }
}
