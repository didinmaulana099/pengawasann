<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use App\Models\permissionDetails;

class RoleController extends Controller
{
    public function getData(){
        $roles = Role::orderBy('id', 'asc')->get();

        return response()->json([
            'type' => 'success',
            'data' => $roles
        ], 200);
    }

    public function index()
    {
        $listRole = Role::orderBy('id', 'asc')->get();
        $listPermission = Permission::orderBy('order_number', 'asc')->get();
         
        return view('admin.settings.role.index', [
            'permission' => $listPermission,
            'list_role' => $listRole
        ]);
    }

    public function store(Request $request)
    {
        $role = new Role();
        $role->nama = $request->roleName;
        $role->keterangan = $request->roleDesc;
        $role->created_by = auth()->user()->nama_lengkap;
        $role->changed_by = auth()->user()->nama_lengkap;
        $role->save();

        foreach (json_decode($request['permission']) as $permission) {
            $permissionDetails = new permissionDetails;
            $permissionDetails->role_id = $role->id;
            $permissionDetails->role_nama = $role->nama;
            $permissionDetails->permission_id = $permission->id;
            $permissionDetails->visible = 1;
            $permissionDetails->save();
        }

        return response()->json([
            'type' => 'success',
            'message' => 'Role Berhasil Ditambahkan!',
        ], 200);
    }

    public function show(Request $request, $id)
    {
        $role = Role::findOrFail($id);

        $permissions = [];
        foreach ($role->permissions as $permission) {
            $permissions[$permission['permission_id']] = $permission['allow'];
        }

        $role->perms = $permissions;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $role = Role::findOrFail($id);

        $permissions = [];

        foreach ($request->permissions as $index => $permission) {
            $permissions[] = [
                'permission_id' => $index,
                'allow' => $permission,
            ];
        }

        $role->name = $request->name;
        $role->description = $request->description;
        $role->permissions = $permissions;
        $role->changed_by = auth()->user()->full_name;
        $role->save();

        return redirect()->route('');
    }

    public function destroy(Request $request, $id)
    {
        $role = Role::where('_id', $id)->delete();
        return redirect()->route('');
    }

    public function list(Request $request)
    {
        $roles = Role::when($request->keyword, function ($query) use ($request) {
            if (!empty($request->keyword)) {
                $query->where('name', 'like', '%' . $request->keyword . '%');
            }
        })
        ->take(10)
        ->get();

        return response()->json(
            [
                'type' => 'success',
                'data' => $roles,
            ],
            200,
        );
    }
}